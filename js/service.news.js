/**
*/
angular.module('news.service.news', ['ngResource'])

    //county
    .service('newsService', ['$resource', function ($resource) {

        var news_resource = newsUrl+"news/";
        //得到所有新闻
        this.getlist = function () {
            var news_all = $resource(news_resource);
            return news_all.query();
        }
        //获取所有新闻列表
        this.getnewssysList = function () {
            var newssys_all = $resource(news_resource);
            return newssys_all.query();
        }
        //通过id获取新闻
        this.getnewssys = function (eid) {
            var news_url = $resource(news_resource+":id");
            return news_url.get({id:eid});
        }
        //添加新闻
        this.addnewssys = function (news,success,error) {
            console.log(news)
            var newssys_url = $resource(news_resource);
            return newssys_url.save(news,success,error);
        }
        //删除新闻
        this.removeNewsByEid = function (eid) {
            var removeExp_url = $resource(news_resource+":id");
            return removeExp_url.remove({id:eid});
        }
        //更新新闻
        this.updateNewsByEid = function (eid,news,success,error) {
            var updateExp_url = $resource(news_resource+":id");
            return updateExp_url.save({id:eid},news,success,error);
        }
    }])
;