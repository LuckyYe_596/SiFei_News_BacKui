/**
 * Created by yeso on 2015/9/24.
 */
angular.module('news.template', [
    'ngCookies'
])
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('news', {
            'abstract': true,
            url: "/exp",
            views: {
                '': {
                    template: '<div ui-view="topbar"></div><div ui-view="main"></div>'
                },
                'topbar@news': {
                    templateUrl: template_viewurl+"/topbar.html",
                    controller: 'GetCookieController'
                },
                'main@news': {
                    templateUrl: template_viewurl+"/main.html",
                    controller: 'newsTemplateController'
                }
            }
        })
    ;
}])

//判断cookie
.controller('GetCookieController', ['$scope','$state','$cookieStore','LoginService', function($scope, $state, $cookieStore, LoginService) {
    //console.log('news GetCookieController');
    /*var data = $cookieStore.get('manager_cookie');
    console.log(data);
    if (data == undefined) {
        alert("Page has expired, please login again");
        $state.go('login');
    } else {
        $scope.manager_cookie = data;
        //console.log("this is" + data.managername);
    }*/
    // topbar
    if (typeof topbars != 'undefined') {
        $scope.topbars = topbars;
    }
}])

//清除cookie
.controller('RemoveCookieController', ['$scope','$state','$cookieStore', function($scope, $state, $cookieStore) {
    console.log('RemoveCookieController');
    $cookieStore.remove('manager_cookie');
    $state.go('home.exit');
}])

//Mall模板
.controller('newsTemplateController', ['$scope','$state', function($scope, $state) {
    //console.log('newsTemplateController');
        $scope.sidebars1=user_sidebars;
        $scope.sidebars2=news_sidebars;


}])
;
