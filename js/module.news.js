angular.module('news.news', [
])
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('news.listuser', {
            url: '/news/list',
            templateUrl: news_viewurl+"/news/userlist.html",
            controller: 'UsersController'
        })

        .state('news.detail', {
            url: '/news/detail/:eId',
            templateUrl: news_viewurl+"/news/detail.html",
            controller: 'newsDetailController'
        })
}])
    .controller('UsersController', ['$scope','newsService', function($scope, newsService) {
        $scope.load = function(){
            newsService.getlist().$promise.then(function(data){
            $scope.news_list = data;
        })}
        $scope.load();
        $scope.remove = function(id) {
            newsService.removeNewsByEid(id).$promise.then(function(data){
                alert("删除成功！");
                $scope.load();
        })}

    }])

.controller('newsDetailController', ['$scope','$state','$stateParams','newsService',function($scope,$state,$stateParams,newsService) {
        console.log($stateParams.eId);
        newsService.getnewssys($stateParams.eId).$promise.then(function (data) {
            $scope.news = data;
        })
        $scope.modify=function(news){
            newsService.updateNewsByEid(news.articleid,news,function(){
                alert("更新成功");
                $state.go("news.listuser");
            },function(){
                alert("更新失败");
                console.log(data);
            })
        }
}])
